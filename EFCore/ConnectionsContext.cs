using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Rimotr
{
    public class ConnectionsContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlite("Data Source=db/connections.db");
        }

        public DbSet<User> Users { get; set; }
        public DbSet<ConnectionLog> Logs { get; set; }
        public DbSet<Computer> Computers { get; set; }
    }
}