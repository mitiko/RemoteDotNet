using System.Linq;

namespace Rimotr
{
    public static class Globals
    {
        public static ConnectionsContext db = new ConnectionsContext();
        public const string apiRoute = "api/v0";
        
        public static User GetUserById(string userId)
        {
            return db.Users.Find(new User(userId));
        }
    }
}