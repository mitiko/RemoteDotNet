using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rimotr.ExtensionMethods
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> Foreach<T>(this IEnumerable<T> items, Action<T> action)
        {
            foreach(var item in items)
            {
                action?.Invoke(item);
                yield return item;
            }
        }
    }
}