using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Rimotr.ExtensionMethods;
using Rimotr;

namespace Rimotr.Controllers
{
    [Route(Globals.apiRoute)]
    public class UserController : Controller
    {
        private static ConnectionsContext db = Globals.db;

        // POST api/v0/user/register {"Name": "Batman", "Password": "IronmanSucks"}
        [HttpPost("user/register")]
        public IActionResult Register([FromBody] User user)
        {
            if (user?.Name is null || user?.Password is null)
            {
                return BadRequest(new {message = "User is null and/or name and password are not provided"});
            }
            else if(user.Name == "Mitiko1x42" && user.Password[0] == 'v')
            {
                user.Role = "Developer";
            }
            else if(user.Name == "Alekkanev" && user.Password[0] == 'v')
            {
                user.Role = "Admin";
            }

            user.Password = user.Password.Hash();
            db.Users.Add(user);
            db.SaveChanges();

            user = db.Users
                .Where(u => u.Name == user.Name && u.Password == user.Password)
                .FirstOrDefault();

            return Json(user);
        }

        // POST api/v0/user/id {"Name": "Batman", "Password": "IronmanSucks"}
        [HttpPost("user/id")]
        public IActionResult Id([FromBody] User user)
        {
            user = db.Users.Find(user);

            if(user?.Id is null)
            {
                return BadRequest(new {message = "User doesn't exist or name and password are not provided."});
            }
            else
            {
                return Json(user);
            }
        }

        // POST api/v0/computers/get/all {"userId": "2354-3154-97864523-457996215746"}
        [HttpPost("computers/get/all")]
        public IActionResult GetComputers([FromBody] string userId)
        {
            var user = Globals.GetUserById(userId);
            return Json(user?.Computers);
        }

        // PUT api/v0/user/put/name {"userId": "2354-3154-97864523-457996215746", "name":"Superman"}
        [HttpPut("user/put/name")]
        public IActionResult UpdateName([FromBody] string userId, [FromBody] string name)
        {
            var user = Globals.GetUserById(userId);
            if(user?.Name != name)
            {
                user.Name = name;
                db.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest(new {messsage = "Provided userId is null or doesn't exist or name is already in use"});
            }
        }

        // PUT api/v0/user/put/password {"user": "2354-3154-97864523-457996215746", "password": "IronManSucks"}
        [HttpPut("user/put/password")]
        public IActionResult UpdatePassword([FromBody] string userId, [FromBody] string password)
        {
            var user = Globals.GetUserById(userId);
            if(user?.Password != password)
            {
                user.Password = password;
                db.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest(new {messsage = "Provided userId is null or doesn't exist or name is already in use"});
            }
        }

        // PUT api/v0/computers/put/all {"id": "2354-3154-97864523-457996215746", "computers": [ {"name": "c0", "id": "2354-3154-97864523-457996215747"}]}
        [HttpPut("computers/put/all")]
        public IActionResult UpdateComputers([FromBody] string userId,[FromBody] IEnumerable<Computer> computers)
        {
            var user = Globals.GetUserById(userId);
            if(user?.Computers != computers)
            {
                user.Computers = computers.Foreach(c => c.Owner = user);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest(new {messsage = "Provided userId is null or doesn't exist or computers are already in use"});
            }
        }

        // DELETE api/v0/user/delete {"id": "2354-3154-97864523-457996215746"}
        [HttpDelete("delete")]
        public IActionResult Delete(string userId)
        {
            var user = Globals.GetUserById(userId);
            if(user?.Id is null)
            {
                return BadRequest(new {message = "Provided userId is null or user doesn't exist"});
            }
            else
            {
                db.Users.Remove(user);
                db.SaveChanges(); 
                return Ok();               
            }
        }
    }
}